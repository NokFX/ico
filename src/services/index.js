import * as _request from './request';
import _history from './history';

export const request = _request;
export const history = _history;