import { Schema, arrayOf, normalize } from 'normalizr';
import { camelizeKeys } from 'humps';
import 'isomorphic-fetch';
import { history } from './history';

const redirectToLoginPage = () => {
  // this long string is for IE, for other browsers window.location.href = Urls.login would be enough
  const baseUrl = `${window.location.protocol}//${window.location.hostname}${
    window.location.port ? `:${window.location.port}` : ''
  }`;
  window.location = `${baseUrl}/auth/login`;
  return Promise.reject({
    error_code: 'invalid_token',
    error_description: 'Unauthorized',
  });
};

function checkTokenRefreshing(resolve) {
  if (!localStorage.getItem('bc_ico_refreshing')) {
    return resolve();
  } else {
    setTimeout(() => checkTokenRefreshing(resolve), 500);
  }
}

function waitTokenRefreshing() {
  return new Promise(resolve => {
    checkTokenRefreshing(resolve);
  });
}

function callApi({ endpoint, method, headers, body }, isExternal = false) {
  const fullUrl = !isExternal ? API_ROOT + endpoint : endpoint;

  const options = {
    method: method || 'GET',
    credentials: 'same-origin',
    headers: headers || {},
    body: body || undefined,
  };

  let preRequest = Promise.resolve();

  if (!isExternal && !options.headers.Authorization) {
    preRequest = waitTokenRefreshing();
  }

  return preRequest
    .then(() => {
      if (!isExternal && !options.headers.Authorization) {
        const token = JSON.parse(localStorage.getItem('bc_ico'));
        if (!token || !token.access_token) {
          return redirectToLoginPage();
        } else {
          options.headers.Authorization = `Bearer ${token.access_token}`;
        }
      }

      return fetch(fullUrl, options);
    })
    .then(response => {
      const contentType = response.headers.get('content-type');
      if (contentType && contentType.includes('application/json')) {
        return response.json().then(json => ({ json, response }));
      }

      if (response.ok) {
        return response.text().then(text => {
          return text
            ? { json: JSON.parse(text), response }
            : { json: {}, response };
        });
      }

      return Promise.reject({
        error_code: 'UNEXPECTED_ERROR',
        error_description: 'Unexpected error',
      });
    })
    .then(({ json, response }) => {
      if (!response.ok) {
        return Promise.reject(json);
      }

      //const camelizedJson = camelizeKeys(json)

      //   return Object.assign({},
      //     normalize(camelizedJson, schema),
      //     { nextPageUrl }
      //   )

      if (!json.status) {
        return json;
      }

      const status = json.status;
      if (status === 'SUCCESS') {
        delete json.status;
        return json;
      }

      delete json.status;
      return Promise.reject(json);
    })
    .then(
      response => response,
      error => {
        if (!error.error_code && !error.error) {
          return Promise.reject({
            error_code: 'UNEXPECTED_ERROR',
            error_description: 'Unexpected error',
          });
        }
        if (error.error_msg) {
          error.error_description = error.error_msg;
        }
        if (
          error.error === 'invalid_token' ||
          error.error_code === 'invalid_token'
        ) {
          localStorage.removeItem('bc_ico');
          redirectToLoginPage();
        }
        return Promise.reject(error);
      }
    );
}

export const get = (endpoint, headers = {}) => {
  return callApi({
    endpoint,
    method: 'GET',
    headers: {
      ...headers,
    },
  });
};

export const externalGet = (endpoint, headers = {}) => {
  return callApi(
    {
      endpoint,
      method: 'GET',
      headers: {
        ...headers,
      },
    },
    true
  );
};

export const jsonPost = (endpoint, data, headers = {}) =>
  callApi({
    endpoint,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      ...headers,
    },
    body: data && JSON.stringify(data),
  });

export const urlencodedPost = (endpoint, data, headers = {}) => {
  const urlencoded =
    data &&
    Object.keys(data).reduce((acc, key) => {
      return acc + `&${key}=${encodeURIComponent(data[key])}`;
    }, '');

  return callApi({
    endpoint,
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      ...headers,
    },
    body: urlencoded.slice(1),
  });
};
