export const getToken = () => {
  const token = JSON.parse(localStorage.getItem('bc_ico'));
  if (!token) {
    throw new Error('Token not found');
  }

  return token;
};

export const getTokenExpiresIn = () => {
  const token = getToken();
  if (!token.expires_in) {
    throw new Error('Token expires_in not found');
  }
  return token.expires_in * 1000;
};

export const getUpdateTokenPeriod = () => {
  const expires_in = getTokenExpiresIn();
  return expires_in - 2000;
};
