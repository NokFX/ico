import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Deposits from './containers/Deposits';
import WalletHistory from './containers/WalletHistory';

const Wallet = ({ match }) => (
 <Switch>
    <Route path={`${match.path}/deposits`} component={Deposits} />
    <Route path={`${match.path}/history`} component={WalletHistory} />
    <Redirect to={`${match.path}/deposits`} />
  </Switch>
);

export default Wallet;