import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Row,
  Col,
  Button,
  Table,
  Card,
  CardHeader,
  CardBody,
  Alert,
  Badge,
  Input,
  FormGroup,
  Label,
} from 'reactstrap';
import memoizee from 'memoizee';
import * as transactionActions from '../../../../features/transactions/actions';
import {
  getIsTransactionsPending,
  getError,
  getTransactions,
} from '../../../../features/transactions/selectors';
import { getTokenCode } from '../../../../features/coinData/selectors';
import Loading from '../../../../components/Loading';

const Status = ({ status, address, txid }) => {
  switch (status) {
    case 0: {
      return (
        <div>
          <Badge style={{ fontSize: '0.875rem' }} color="warning">
            Processing
          </Badge>
          <br />
          <span>
            <small>
              Address: <strong>{address}</strong>
            </small>
          </span>
          <br />
          <span>
            <small>
              Txid: <strong>{txid}</strong>
            </small>
          </span>
        </div>
      );
    }
    case 1: {
      return (
        <div>
          <Badge style={{ fontSize: '0.875rem' }} color="success">
            Completed
          </Badge>
          <br />
          {/* <span>Completed</span><br/> */}
          <span>
            <small>
              Address: <strong>{address}</strong>
            </small>
          </span>
          <br />
          <span>
            <small>
              Txid: <strong>{txid}</strong>
            </small>
          </span>
        </div>
      );
    }
    default: {
      <div>
        <Badge style={{ fontSize: '0.875rem' }} color="secondary">
          Unknown
        </Badge>
      </div>;
    }
  }
};

const getTransactionsByType = memoizee((transacts, type) => {
  return transacts.filter(t => t.type === type);
});

class History extends Component {
  state = {
    typeValue: 'DEPOSIT',
  };

  componentDidMount() {
    this.props.transactionActions.getTransactionsAct();
  }

  handleTypeChange = ev => {
    if (this.props.error) {
      this.props.transactionActions.getTransactionsAct();
    }
    this.setState({ typeValue: ev.target.value });
  };

  render() {
    const {
      transactions,
      isTransactionsPending,
      error,
      tokenCode,
    } = this.props;
    const transactionsByType = getTransactionsByType(
      transactions,
      this.state.typeValue
    );

    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>History</CardHeader>
          <CardBody>
            {/* <FormGroup row>
              <Col xs="12" sm="4" md="3">
                <Input 
                  value={this.state.typeValue}
                  type="select"
                  name="select"
                  id="select"
                  onChange={this.handleTypeChange}
                >
                  <option value="DEPOSIT">Deposit</option>
                  <option value="WITHDRAWAL">Withdrawal</option>
                </Input>
              </Col>
            </FormGroup> */}
            <Table responsive>
              <thead>
                <tr>
                  <th>Status</th>
                  <th>Date time</th>
                  <th>Coin</th>
                  <th>Amount</th>
                </tr>
              </thead>
              <tbody>
                {error && (
                  <tr>
                    <td colSpan="4">
                      <Alert color="danger" className="text-center">
                        <span>{error.error_description}</span>
                      </Alert>
                    </td>
                  </tr>
                )}
                {!error &&
                  isTransactionsPending && (
                    <tr>
                      <td colSpan="4">
                        <Loading />
                      </td>
                    </tr>
                  )}
                {!error &&
                  !isTransactionsPending &&
                  transactionsByType.length === 0 && (
                    <tr>
                      <td colSpan="4">
                        <Alert color="info" className="text-center">
                          <span>There are no transactions yet</span>
                        </Alert>
                      </td>
                    </tr>
                  )}
                {!error &&
                  !isTransactionsPending &&
                  transactionsByType.length > 0 &&
                  transactions.map(transact => {
                    if (transact.amount) {
                      return (
                        <tr key={transact.id}>
                          <td className="align-middle">
                            <Status
                              status={transact.status}
                              address={transact.addressSender}
                              txid={transact.addressTX}
                            />
                          </td>
                          <td className="align-middle">{transact.date}</td>
                          <td className="align-middle">{tokenCode}</td>
                          <td className="align-middle">{transact.amount}</td>
                        </tr>
                      );
                    }
                    return null;
                  })}
              </tbody>
            </Table>
          </CardBody>
        </Card>
      </div>
    );
  }
}

const stateToProps = state => ({
  isTransactionsPending: getIsTransactionsPending(state),
  error: getError(state),
  transactions: getTransactions(state),
  tokenCode: getTokenCode(state),
});

const dispatchToProps = dispatch => ({
  transactionActions: bindActionCreators(transactionActions, dispatch),
});

export default connect(stateToProps, dispatchToProps)(History);
