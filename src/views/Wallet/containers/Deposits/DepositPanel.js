import React, { Component } from 'react';
import LaddaButton, {SLIDE_LEFT} from 'react-ladda';
import {
    Alert,
} from 'reactstrap';

const DepositPanel = ({ 
    ethereumWallet, 
 }) => {
    // if (!ethereumWallet) {
    //     return (
    //         <div>
    //             <Alert color="info" className="text-center">
    //                 <span>Click Generate to create your Ethereum wallet</span>
    //             </Alert>
    //             <div className="row">
    //                 <div style={{margin: 'auto'}}>
    //                     <LaddaButton
    //                         className="btn btn-primary btn-ladda ml-3"
    //                         loading={isWalletLoading}
    //                         data-color="green"
    //                         data-style={SLIDE_LEFT}
    //                         onClick={openGenWalletPinWnd}
    //                     >
    //                         Generate
    //                     </LaddaButton>
    //                 </div>
    //             </div>
    //         </div>
    //     );
    // }
    return (
        <div>
            <p>Your Ethereum Deposit Address</p>
            <Alert color="info" className="text-center">
                <span><strong>{ethereumWallet}</strong></span>
            </Alert>
            <p className="text-danger">
                The minimum amount for deposit is 0.05 ETH<br/>
                IMPORTANT: Send only Ether (ETH) to this deposit address.
            </p>
            <p>Sending tokens or any other currency to this address will result in the permanent loss of your deposit.</p>
        </div>
    );
}

export default DepositPanel;
