import React, { Component } from 'react';
import LaddaButton, {SLIDE_LEFT} from 'react-ladda';
import {
    Table,
    Row,
    Col,
  } from 'reactstrap';

const styles = {
    titleDiv: {
        display: "inline-block",
        width: 120,
    },
    valueDiv: {
        display: "inline-block",
    }
}  

const DepositSmall = ({
    handleOpenDeposit,
    amount,
    isWalletLoading,
    openGenWalletPinWnd,
    ethereumWallet,
}) => (
  <Row>
      <Col className="col-sm-6">
        <div className="mb-3">
            <div style={styles.titleDiv}><strong>Coin:</strong></div>
            <div style={styles.valueDiv}>ETH</div>
        </div>
        <div className="mb-3">
            <div style={styles.titleDiv}><strong>Name:</strong></div>
            <div style={styles.valueDiv}>Ethereum</div>
        </div>
        <div className="mb-3">
            <div style={styles.titleDiv}><strong>Total Coin:</strong></div>
            <div style={styles.valueDiv}>{amount}</div>
        </div>
        <div className="mb-3">
            {ethereumWallet &&  
                <LaddaButton
                    className="btn btn-primary btn-ladda"
                    loading={false}
                    onClick={handleOpenDeposit}
                    data-color="green"
                    data-style={SLIDE_LEFT}
                >
                    Deposit
                </LaddaButton>
            }

            {!ethereumWallet && 
                <LaddaButton
                    className="btn btn-primary btn-ladda"
                    loading={isWalletLoading}
                    data-color="green"
                    data-style={SLIDE_LEFT}
                    onClick={openGenWalletPinWnd}
                >
                    Generate wallet
                </LaddaButton> 
            }
            {/* <LaddaButton
                className="btn btn-secondary btn-ladda ml-3"
                loading={false}
                data-color="green"
                data-style={SLIDE_LEFT}
            >
                Withdraw
            </LaddaButton> */}
        </div>
      </Col>
  </Row>
);

export default DepositSmall;