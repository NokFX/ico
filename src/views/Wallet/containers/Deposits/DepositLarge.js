import React, { Component } from 'react';
import LaddaButton, {SLIDE_LEFT} from 'react-ladda';
import {
    Table,
  } from 'reactstrap';

const DepositLarge = ({
    handleOpenDeposit,
    amount,
    isWalletLoading,
    openGenWalletPinWnd,
    ethereumWallet,
}) => (
    <Table className="hidden-sm-down">
    <thead>
      <tr>
        <th className="center">Coin</th>
        <th>Name</th>
        <th>Total Coin</th>
        <th className="center">Actions</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td className="center align-middle">
          <img className="icon-image-25" src="/img/eth.png" alt="eth" /> ETH
        </td>
        <td className="left align-middle">Ethereum</td>
        <td className="left align-middle">{amount}</td>
        <td className="align-middle">
          {ethereumWallet && 
            <LaddaButton
                className="btn btn-primary btn-ladda"
                loading={false}
                onClick={handleOpenDeposit}
                data-color="green"
                data-style={SLIDE_LEFT}
            >
                Deposit
            </LaddaButton>
          }

          {!ethereumWallet && 
            <LaddaButton
                className="btn btn-primary btn-ladda"
                loading={isWalletLoading}
                data-color="green"
                data-style={SLIDE_LEFT}
                onClick={openGenWalletPinWnd}
            >
                Generate wallet
            </LaddaButton> 
          }

          {/* <LaddaButton
              className="btn btn-secondary btn-ladda ml-3"
              loading={false}
              data-color="green"
              data-style={SLIDE_LEFT}
          >
              Withdraw
          </LaddaButton> */}
        </td>
      </tr>
    </tbody>
  </Table>
);

export default DepositLarge;