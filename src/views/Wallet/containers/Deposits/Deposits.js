import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    Row,
    Col,
    Button,
    Table,
    Card,
    CardHeader,
    CardBody,
    Collapse,
    Alert,
  } from 'reactstrap';
import LaddaButton, {SLIDE_LEFT} from 'react-ladda';
import * as walletActions from '../../../../features/wallet/actions';
import DepositPanel from './DepositPanel';
import DepositLarge from './DepositLarge';
import DepositSmall from './DepositSmall';
import { getEtherBalance, getEthereumWallet } from '../../../../features/wallet/selectors';
import PinDialog from '../../../../components/Dialogs/PinDialog';

class Deposits extends Component {
  state = {
    depositActive: false,
    genWalletPinOpen: false,
  }

  handleOpenDeposit = () => {
    this.setState({ depositActive: true })
  }

  closeGenWalletPinWnd = () => {
      this.setState({genWalletPinOpen: false});
  }

  openGenWalletPinWnd = () => {
      this.setState({genWalletPinOpen: true});
  }

  handleGenWallet = (pin) => {
    this.props.walletActions.generateWalletAct(pin);
    this.setState({genWalletPinOpen: false});
  }

  render() {
    const { isWalletLoading, ethereumWallet, browser, amount } = this.props;

    return (
      <div className="animated fadeIn">
        <Card>
            <CardHeader>
                Deposit
            </CardHeader>
            <CardBody>

          {browser.lessThan.medium 
            ?
              <DepositSmall 
                handleOpenDeposit={this.handleOpenDeposit}
                amount={amount}
                isWalletLoading={isWalletLoading}
                openGenWalletPinWnd={this.openGenWalletPinWnd}
                ethereumWallet={ethereumWallet}
              />
            :
              <DepositLarge 
                handleOpenDeposit={this.handleOpenDeposit}
                amount={amount}
                isWalletLoading={isWalletLoading}
                openGenWalletPinWnd={this.openGenWalletPinWnd}
                ethereumWallet={ethereumWallet}
              />
          }

          <Collapse isOpen={this.state.depositActive}>
            <div className="border border-right-0 border-left-0 pt-3 pb-3">
                <DepositPanel 
                    ethereumWallet={ethereumWallet} 
                />
            </div>
          </Collapse>
        </CardBody>
       </Card>
       {this.state.genWalletPinOpen && 
        <PinDialog 
            isOpen
            handleClose={this.closeGenWalletPinWnd}
            handleOk={this.handleGenWallet}
        /> }
      </div>
    )
  }
}
const stateToProps = (state) => ({
  isWalletLoading: state.wallet.isWalletLoading,
  browser: state.browser,
  amount: getEtherBalance(state),
  ethereumWallet: getEthereumWallet(state),
});

const dispatchToProps = dispatch => ({
  walletActions: bindActionCreators(walletActions, dispatch)
});

export default connect(stateToProps, dispatchToProps)(Deposits)
