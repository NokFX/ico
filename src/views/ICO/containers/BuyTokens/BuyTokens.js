import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Row,
  Col,
  Button,
  Table,
  Card,
  CardHeader,
  CardBody,
  Collapse,
  Alert,
  InputGroup,
  InputGroupAddon,
  Input,
  FormGroup,
  Label,
} from 'reactstrap';
import LaddaButton, { SLIDE_LEFT, EXPAND_RIGHT } from 'react-ladda';
import { Link } from 'react-router-dom';
import * as walletActions from '../../../../features/wallet/actions';
import {
  getEtherBalance,
  getEthereumWallet,
} from '../../../../features/wallet/selectors';
import {
  getTokenCode,
  getEtherTokenPrice,
} from '../../../../features/coinData/selectors';
import PinDialog from '../../../../components/Dialogs/PinDialog';
import { Decimal } from 'decimal.js';

import 'spinkit/css/spinkit.css';

class BuyTokens extends Component {
  state = {
    amount: '',
    total: '0.000000',
    buyPinOpen: false,
    error: '',
  };

  handleOpenDeposit = () => {
    this.setState({ depositActive: true });
  };

  closeBuyPinWnd = () => {
    this.setState({ buyPinOpen: false });
  };

  openBuyPinWnd = () => {
    const dAmount = new Decimal(this.state.total);
    const dbalance = new Decimal(this.props.balance);
    if (dAmount.gt(dbalance)) {
      this.setState({ error: 'Insufficient ETH balance' });
    } else {
      this.setState({ buyPinOpen: true });
    }
  };

  handleBuy = pin => {
    this.props.walletActions.buyTokensAct({
      pin,
      tokenAmount: this.state.amount,
      ethAmount: this.state.total,
    });
    this.setState({
      buyPinOpen: false,
      amount: 0,
      total: '0.000000',
    });
  };

  handleAmountChange = ev => {
    const isFloat = /^\d*(\.\d*)?$/.test(ev.target.value);
    if (isFloat) {
      let rezAmount = ev.target.value;
      if (ev.target.value.indexOf('.') > -1) {
        const amountParts = ev.target.value.split('.');
        let decimalPart = '';
        if (amountParts[1]) {
          if (amountParts[1].length > 6) {
            decimalPart = amountParts[1].substr(0, 6);
          } else {
            decimalPart = amountParts[1];
          }
        }
        rezAmount = `${amountParts[0]}.${decimalPart}`;
      }

      const rezAmountToDecimal = rezAmount
        ? rezAmount === '.' ? 0 : rezAmount
        : 0;
      const dAmount = new Decimal(rezAmountToDecimal);
      const dTotal = dAmount.times(this.props.tokenPrice);

      this.setState({
        amount: rezAmount,
        total: dTotal.toFixed(6).toString(),
        error: '',
      });
    }
  };

  render() {
    const {
      isWalletLoading,
      ethereumWallet,
      browser,
      balance,
      tokenCode,
      tokenPrice,
      isBuyingPending,
      buyTokenError,
    } = this.props;

    if (!ethereumWallet) {
      return (
        <div className="animated fadeIn">
          <Card>
            <CardHeader>Buy Tokens</CardHeader>
            <CardBody>
              <Alert color="info" className="text-center">
                <span>
                  In order to buy tokens you need to create a wallet{' '}
                  <Link to="/app/wallet/deposits">here</Link>
                </span>
              </Alert>
            </CardBody>
          </Card>
        </div>
      );
    }

    return (
      <div className="animated fadeIn">
        <Card>
          <CardHeader>Buy Tokens</CardHeader>
          <CardBody>
            <Row>
              <Col xs="12" sm="6" className="mt-3">
                {/* <div className="mb-3">
                            <h5>1 {tokenCode} = 1 ETH</h5>
                        </div> */}
                {buyTokenError && (
                  <Alert color="danger" className="text-center">
                    <span>{buyTokenError.error_description}</span>
                  </Alert>
                )}
                {this.state.error && (
                  <Alert color="danger" className="text-center">
                    <span>{this.state.error}</span>
                  </Alert>
                )}
                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="balance">Balance</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <InputGroup>
                      <Input
                        disabled
                        type="text"
                        name="balance"
                        value={balance}
                        placeholder="0.000000"
                      />
                      <InputGroupAddon>ETH</InputGroupAddon>
                    </InputGroup>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="price">Price</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <InputGroup>
                      <Input
                        disabled
                        type="text"
                        name="price"
                        value={tokenPrice}
                        placeholder="0.000000"
                      />
                      <InputGroupAddon>ETH</InputGroupAddon>
                    </InputGroup>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="token_count">Amount</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <InputGroup>
                      <Input
                        value={this.state.amount}
                        type="text"
                        name="token_count"
                        onChange={this.handleAmountChange}
                        placeholder="0"
                      />
                      <InputGroupAddon>{tokenCode}</InputGroupAddon>
                    </InputGroup>
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Col md="3">
                    <Label htmlFor="total">Total</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <InputGroup>
                      <Input
                        type="text"
                        name="total"
                        disabled
                        value={this.state.total}
                        placeholder="0.000000"
                      />
                      <InputGroupAddon>ETH</InputGroupAddon>
                    </InputGroup>
                  </Col>
                </FormGroup>

                <LaddaButton
                  className="btn btn-primary btn-ladda mt-1"
                  loading={isBuyingPending}
                  data-color="green"
                  data-style={EXPAND_RIGHT}
                  onClick={this.openBuyPinWnd}
                  disabled={!parseFloat(this.state.total)}
                >
                  Buy
                </LaddaButton>
              </Col>
            </Row>
          </CardBody>
          {this.state.genWalletPinOpen && (
            <PinDialog
              isOpen
              handleClose={this.closeGenWalletPinWnd}
              handleOk={this.handleGenWallet}
            />
          )}
        </Card>
        {this.state.buyPinOpen && (
          <PinDialog
            isOpen
            handleClose={this.closeBuyPinWnd}
            handleOk={this.handleBuy}
          />
        )}
      </div>
    );
  }
}
const stateToProps = state => ({
  isWalletLoading: state.wallet.isWalletLoading,
  isBuyingPending: state.wallet.isBuyingPending,
  buyTokenError: state.wallet.buyTokenError,
  browser: state.browser,
  balance: getEtherBalance(state),
  ethereumWallet: getEthereumWallet(state),
  tokenCode: getTokenCode(state),
  tokenPrice: getEtherTokenPrice(state),
});

const dispatchToProps = dispatch => ({
  walletActions: bindActionCreators(walletActions, dispatch),
});

export default connect(stateToProps, dispatchToProps)(BuyTokens);
