import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import BuyTokens from './containers/BuyTokens';
// TODO перенсти компонент в папку ICO
import WalletHistory from '../Wallet/containers/WalletHistory';

const Wallet = ({ match }) => (
 <Switch>
    <Route path={`${match.path}/buy_tokens`} component={BuyTokens} />
    <Route path={`${match.path}/history`} component={WalletHistory} />
    <Redirect to={`${match.path}/buy_tokens`} />
  </Switch>
);

export default Wallet;