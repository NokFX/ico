import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  Alert,
} from 'reactstrap';
import * as R from 'ramda';
import LaddaButton, { SLIDE_LEFT } from 'react-ladda';
import * as authActions from '../../features/auth/actions';
import {
  getIsVerifyEmailPending,
  getVerifyEmailError,
  getIsEmailVerified,
  getIsRegisterPending,
  getRegisterError,
} from '../../features/auth/selectors';

class Register extends Component {
  state = {
    email: '',
    password: '',
    passwordConfirm: '',
    errors: [],
    code: '',
  };

  handleEmailChange = ev => {
    this.setState({
      email: ev.target.value,
      errors: [],
    });
  };

  handlePasswordChange = ev => {
    this.setState({
      password: ev.target.value,
      errors: [],
    });
  };

  handlePasswordConfirmChange = ev => {
    this.setState({
      passwordConfirm: ev.target.value,
      errors: [],
    });
  };

  handleCodeChange = ev => {
    this.setState({
      code: ev.target.value,
    });
  };

  handleCodeSubmit = ev => {
    this.props.authActions.registerAct(this.state.code);
  };

  handleSubmit = () => {
    const errors = [];
    const { email, password, passwordConfirm } = this.state;
    if (!R.trim(email)) {
      errors.push('Email required');
    }
    const pwd = R.trim(password);
    if (!pwd) {
      errors.push('Password required');
    }
    const pwdConfirm = R.trim(passwordConfirm);
    if (!pwdConfirm) {
      errors.push('Password confirmation required');
    }

    if (pwd && pwdConfirm && pwd !== pwdConfirm) {
      errors.push("Password and Password confirmation don't match");
    }
    if (errors.length > 0) {
      this.setState({ errors });
    } else {
      this.props.authActions.verifyEmailAct({ email, password });
    }
  };

  render() {
    const {
      isVerifyEmailPending,
      verifyEmailError,
      isEmailVerified,
      isRegisterPending,
      registerError,
    } = this.props;

    if (isEmailVerified) {
      return (
        <div className="app flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="6">
                <Card className="mx-4">
                  <CardBody className="p-4">
                    <h1>Confirm your email</h1>
                    <p className="text-muted">Enter code from email</p>
                    {registerError && (
                      <Alert color="danger" className="text-center">
                        <span>{registerError.error_description}</span>
                      </Alert>
                    )}
                    <InputGroup className="mb-3">
                      <Input
                        type="text"
                        placeholder="Code"
                        disabled={isRegisterPending}
                        onChange={this.handleCodeChange}
                      />
                    </InputGroup>
                    <LaddaButton
                      className="btn btn-success btn-ladda w-100"
                      loading={isRegisterPending}
                      data-color="success"
                      data-style={SLIDE_LEFT}
                      onClick={this.handleCodeSubmit}
                      disabled={!this.state.code}
                    >
                      Verify email
                    </LaddaButton>
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Container>
        </div>
      );
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  {verifyEmailError && (
                    <Alert color="danger" className="text-center">
                      <span>{verifyEmailError.error_description}</span>
                    </Alert>
                  )}
                  {this.state.errors.length > 0 && (
                    <Alert color="danger">
                      {this.state.errors.map(err => <div key={err}>{err}</div>)}
                    </Alert>
                  )}
                  <InputGroup className="mb-3">
                    <InputGroupAddon>@</InputGroupAddon>
                    <Input
                      type="text"
                      placeholder="Email"
                      disabled={isVerifyEmailPending}
                      onChange={this.handleEmailChange}
                    />
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon>
                      <i className="icon-lock" />
                    </InputGroupAddon>
                    <Input
                      type="password"
                      placeholder="Password"
                      disabled={isVerifyEmailPending}
                      onChange={this.handlePasswordChange}
                    />
                  </InputGroup>
                  <InputGroup className="mb-4">
                    <InputGroupAddon>
                      <i className="icon-lock" />
                    </InputGroupAddon>
                    <Input
                      type="password"
                      placeholder="Repeat password"
                      disabled={isVerifyEmailPending}
                      onChange={this.handlePasswordConfirmChange}
                    />
                  </InputGroup>
                  <LaddaButton
                    className="btn btn-success btn-ladda w-100"
                    loading={isVerifyEmailPending}
                    data-color="success"
                    data-style={SLIDE_LEFT}
                    onClick={this.handleSubmit}
                  >
                    Create Account
                  </LaddaButton>

                  <p className="text-center mt-3 mb-0">
                    <span>Already have an account?</span>{' '}
                    <Link to="/auth/login">Login</Link>
                  </p>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const stateToProps = state => ({
  isVerifyEmailPending: getIsVerifyEmailPending(state),
  verifyEmailError: getVerifyEmailError(state),
  isEmailVerified: getIsEmailVerified(state),
  isRegisterPending: getIsRegisterPending(state),
  registerError: getRegisterError(state),
});

const dispatchToProps = dispatch => ({
  authActions: bindActionCreators(authActions, dispatch),
});

export default connect(stateToProps, dispatchToProps)(Register);
