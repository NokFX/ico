import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Container,
  Row,
  Alert,
  Col,
  CardGroup,
  Card,
  CardBody,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
  FormGroup,
} from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../features/auth/actions';
import * as navigateActions from '../../features/navigation/actions';
import * as R from 'ramda';
import LaddaButton, { SLIDE_RIGHT } from 'react-ladda';

const ErrorPanel = ({ errors }) => {
  return (
    <div>
      {Object.values(errors).map(error => {
        if (!!error) {
          return <div className="text-danger">{error}</div>;
        }
        return null;
      })}
    </div>
  );
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      errors: {},
    };
  }

  handleSubmit = () => {
    const errors = {};
    if (!R.trim(this.state.login)) {
      errors.login = 'Login required';
    }
    if (!R.trim(this.state.password)) {
      errors.password = 'Password required';
    }
    if (Object.keys(errors).length > 0) {
      this.setState({ errors });
    } else {
      this.props.login(this.state);
    }
  };

  handleLoginChange = ev => {
    this.setState({
      login: ev.target.value,
      errors: { ...this.state.errors, login: undefined },
    });
  };

  handlePasswordChange = ev => {
    this.setState({
      password: ev.target.value,
      errors: { ...this.state.errors, password: undefined },
    });
  };

  handleRegister = ev => {
    this.props.navigate('/auth/register');
  };

  render() {
    const { isLoginPending } = this.props;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup className="mb-4">
                <Card className="p-4">
                  <CardBody>
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    {this.props.auth_error && (
                      <Alert color="danger" className="text-center">
                        <span>{this.props.auth_error.error_description}</span>
                      </Alert>
                    )}
                    {this.state.errors.login && (
                      <span className="text-danger">
                        {this.state.errors.login}
                      </span>
                    )}
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="icon-user" />
                      </InputGroupAddon>
                      <Input
                        disabled={isLoginPending}
                        type="text"
                        placeholder="Username"
                        onChange={this.handleLoginChange}
                        required
                      />
                    </InputGroup>
                    {this.state.errors.password && (
                      <span className="text-danger">
                        {this.state.errors.password}
                      </span>
                    )}
                    <InputGroup className="mb-4">
                      <InputGroupAddon>
                        <i className="icon-lock" />
                      </InputGroupAddon>
                      <Input
                        disabled={isLoginPending}
                        type="password"
                        placeholder="Password"
                        onChange={this.handlePasswordChange}
                        required
                      />
                    </InputGroup>

                    <Row>
                      <Col xs="6">
                        <LaddaButton
                          className="btn btn-primary btn-ladda px-4"
                          loading={isLoginPending}
                          onClick={this.handleSubmit}
                          data-style={SLIDE_RIGHT}
                        >
                          Login
                        </LaddaButton>
                      </Col>
                      {/* <Col xs="6" className="text-right">
                        <Button
                          disabled={isLoginPending}
                          color="link"
                          className="px-0"
                        >
                          Forgot password?
                        </Button>
                      </Col> */}
                      <Col
                        xs="6"
                        className="text-right d-block d-sm-block d-md-block d-lg-none"
                      >
                        <Button
                          disabled={isLoginPending}
                          color="link"
                          className="px-0"
                        >
                          <Link to="/auth/register">Register</Link>
                        </Button>
                      </Col>
                    </Row>
                  </CardBody>
                </Card>
                <Card
                  className="text-white bg-primary py-5 d-md-down-none"
                  style={{ width: 44 + '%' }}
                >
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>
                        By signing up, you agree to our Terms & Privacy Policy
                      </p>
                      <Button
                        disabled={isLoginPending}
                        color="primary"
                        className="mt-3"
                        onClick={this.handleRegister}
                        active
                      >
                        Register Now!
                      </Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const stateToProps = ({ auth }) => ({
  isLoginPending: auth.isLoginPending,
  authenticated: auth.authenticated,
  auth_error: auth.error,
});

export default connect(stateToProps, { ...actions, ...navigateActions })(Login);
