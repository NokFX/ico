import React, { Component } from 'react';
import { connect } from 'react-redux';
import CurrencyWidget from '../../components/Widgets/CurrencyWidget';
import { Row, Col, CardGroup, Card, CardBody } from 'reactstrap';
import {
  getEtherBalance,
  getWeiTokenBalance,
} from '../../features/wallet/selectors';
import {
  getTokenCode,
  getEtherTotalTokenSupply,
  getTokenRemainder,
} from '../../features/coinData/selectors';

class Dashboard extends Component {
  render() {
    const {
      amount,
      tokenBalance,
      tokenCode,
      totalTokenSupply,
      tokenRemainder,
    } = this.props;
    console.log('totalTokenSupply', totalTokenSupply);
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="3">
            <CurrencyWidget
              header={amount}
              mainText="ETH"
              icon="fa fa-moon-o"
              color="info"
              imageSrc="/img/eth.png"
            />
          </Col>
          <Col xs="12" sm="6" lg="3">
            <CurrencyWidget
              header={tokenBalance}
              mainText={tokenCode}
              icon="fa fa-rocket"
              color="info"
            />
          </Col>
        </Row>
        <Row>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-primary">
              <CardBody className="pb-0">
                <h4 className="mb-0">{totalTokenSupply}</h4>
                <p>Token supply</p>
              </CardBody>
              <div className="chart-wrapper px-3" style={{ height: '70px' }} />
            </Card>
          </Col>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <h4 className="mb-0">{tokenRemainder}</h4>
                <p>Current token supply</p>
              </CardBody>
              <div className="chart-wrapper px-3" style={{ height: '70px' }} />
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const stateToProps = state => ({
  amount: getEtherBalance(state),
  tokenCode: getTokenCode(state),
  tokenBalance: getWeiTokenBalance(state),
  totalTokenSupply: getEtherTotalTokenSupply(state),
  tokenRemainder: getTokenRemainder(state),
});

export default connect(stateToProps)(Dashboard);
