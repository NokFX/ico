import React, {Component} from 'react';
import {
    InputGroupAddon,
    Input, InputGroup, Row, Col, Button, Modal, ModalHeader, ModalBody, ModalFooter, Card, CardHeader, CardBody} from 'reactstrap';
import * as R from 'ramda';

class PinDialog extends Component {
  state = {
      pin: '',
  }  

  handlePinChange = (ev) => {
    this.setState({ pin: ev.target.value });
  }; 

  handleClose = () => {
      this.props.handleClose();
  };

  handleOk = () => {
    this.props.handleOk(R.trim(this.state.pin));
  };

  render() {
    return (
        <Modal isOpen={this.props.isOpen} toggle={this.handleClose} className={this.props.className}>
            <ModalHeader toggle={this.handleClose}>Enter pin code</ModalHeader>
            <ModalBody>
                <InputGroup>
                    <InputGroupAddon><i className="icon-lock"></i></InputGroupAddon>
                    <Input 
                        type="password"
                        placeholder="Pin"
                        onChange={this.handlePinChange} 
                        required
                    />
                </InputGroup>
            </ModalBody>
            <ModalFooter>
                <Button 
                    color="primary"
                    onClick={this.handleOk}
                    disabled={!this.state.pin}
                >
                    Ok
                </Button>
                <Button color="secondary" onClick={this.handleClose}>Cancel</Button>
            </ModalFooter>
        </Modal>
    )
  }
}

export default PinDialog;
