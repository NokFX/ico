export default {
  items: [
    {
      name: 'Dashboard',
      url: '/app/dashboard',
      icon: 'icon-speedometer',
    },
    {
      name: 'Wallet',
      icon: 'icon-wallet',
      url: '/app/wallet',
      children: [
        {
          name: 'Deposit',
          url: '/app/wallet/deposits',
        },
        // {
        //   name: 'History',
        //   url: '/app/wallet/history',
        // }
      ]
    },
    {
      name: 'ICO',
      icon: 'icon-briefcase',
      url: '/app/ico',
      children: [
        {
          name: 'Buy tokens',
          url: '/app/ico/buy_tokens',
        },
        {
          name: 'History',
          url: '/app/ico/history',
        }
      ]
    }
  ]
};
