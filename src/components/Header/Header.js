import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Nav, NavItem, NavLink, NavbarToggler, NavbarBrand } from 'reactstrap';
import * as authActions from '../../features/auth/actions';
import {
  getETHViewPrice,
  getUsdTokenPrice,
  getTokenCode,
} from '../../features/coinData/selectors';

class Header extends Component {
  sidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-hidden');
  }

  sidebarMinimize(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-minimized');
  }

  mobileSidebarToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('sidebar-mobile-show');
  }

  asideToggle(e) {
    e.preventDefault();
    document.body.classList.toggle('aside-menu-hidden');
  }

  handleLogout = () => {
    this.props.logout();
  };

  render() {
    const { ethPrice, tokenUsdPrice, tokenCode } = this.props;
    return (
      <header className="app-header navbar">
        <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
          <span className="navbar-toggler-icon" />
        </NavbarToggler>
        <NavbarBrand href="#" />
        <NavbarToggler
          className="d-md-down-none mr-auto"
          onClick={this.sidebarToggle}
        >
          <span className="navbar-toggler-icon" />
        </NavbarToggler>
        <span className="d-md-down-none">
          <span className="mr-3" style={{ color: '#fff' }}>
            <strong>
              1 {tokenCode} = {tokenUsdPrice} $
            </strong>
          </span>
          <span className="mr-3" style={{ color: '#fff' }}>
            |
          </span>
          <span className="mr-3" style={{ color: '#fff' }}>
            <strong>1 ETH = {ethPrice} $</strong>
          </span>
        </span>
        <Nav navbar>
          <NavItem className="px-3">
            <NavLink href="#" onClick={this.handleLogout}>
              Logout
            </NavLink>
          </NavItem>
        </Nav>
        {/* <NavbarToggler className="d-md-down-none" onClick={this.asideToggle}>
          <span className="navbar-toggler-icon"></span>
        </NavbarToggler> */}
      </header>
    );
  }
}

const stateToProps = state => ({
  ethPrice: getETHViewPrice(state),
  tokenUsdPrice: getUsdTokenPrice(state),
  tokenCode: getTokenCode(state),
});

export default connect(stateToProps, { ...authActions })(Header);
