import { take, call, put, cancel, takeEvery } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Decimal } from 'decimal.js';
import { toast } from 'react-toastify';
import * as api from './api';
import { getWalletAct, generateWalletAct, buyTokensAct } from './actions';

function* generateWallet(actionBody) {
  try {
    const pin = actionBody.payload;
    yield put(generateWalletAct.request());
    const response = yield call(api.generateWalletRequest, pin);
    yield put(generateWalletAct.success(response.address));
    toast.success('Wallet has generated successfully!');
  } catch (error) {
    yield put(generateWalletAct.failure(error));
  } finally {
    yield put(generateWalletAct.fulfill());
  }
}

function* buyTokens(actionBody) {
  try {
    const { pin, tokenAmount, ethAmount } = actionBody.payload;
    const dTokenAmount = new Decimal(tokenAmount);
    //const dWeiTokenAmount = dTokenAmount.times('1000000000000000000');
    const dEthAmount = new Decimal(ethAmount);
    const dWeiEthAmount = dEthAmount.times('1000000000000000000');

    yield put(buyTokensAct.request());
    const response = yield call(api.buyTokensRequest, pin, dWeiEthAmount);

    toast.success(`You've successfully purchased ${tokenAmount} tokens`);
    // TODO возможно в дальнейшем брать значение из response
    yield put(
      buyTokensAct.success({
        tokenAmount: tokenAmount,
        ethAmount: dWeiEthAmount.toString(),
      })
    );
  } catch (error) {
    yield put(buyTokensAct.failure(error));
  } finally {
    yield put(buyTokensAct.fulfill());
  }
}

export function* watchWallet() {
  yield takeEvery(generateWalletAct.TRIGGER, generateWallet);
  yield takeEvery(buyTokensAct.TRIGGER, buyTokens);
}
