import { generateWalletAct, setWalletData, buyTokensAct } from './actions';
import { Decimal } from 'decimal.js';

const initialState = {
  isWalletLoading: false,
  error: undefined,
  ethereumBalance: 0,
  ethereumWallet: undefined,
  tokenBalance: 0,
  tokenCode: '',
  tokenName: '',

  buyTokenError: undefined,
  isBuyingPending: false,
};

export default function walletReducer(state = initialState, action) {
  switch (action.type) {
    case generateWalletAct.TRIGGER:
      return {
        ...state,
        isWalletLoading: true,
      };
    case generateWalletAct.SUCCESS:
      return {
        ...state,
        ethereumBalance: 0,
        ethereumWallet: action.payload,
        error: undefined,
      };
    case generateWalletAct.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case generateWalletAct.FULFILL:
      return {
        ...state,
        isWalletLoading: false,
      };

    case 'SET_WALLET_DATA': {
      const {
        ethereumBalance,
        ethereumWallet,
        tokenBalance,
        tokenCode,
        tokenName,
        totalSupply,
        usdPrice,
      } = action.payload;
      return {
        ...state,
        ethereumBalance,
        ethereumWallet,
        tokenBalance,
        tokenCode,
        tokenName,
        totalSupply,
        usdTokenPrice: usdPrice,
      };
    }

    case buyTokensAct.TRIGGER:
      return {
        ...state,
        isBuyingPending: true,
        buyTokenError: undefined,
      };
    case buyTokensAct.SUCCESS:
      return {
        ...state,
        ethereumBalance: new Decimal(state.ethereumBalance).sub(
          new Decimal(action.payload.ethAmount)
        ),
        tokenBalance: new Decimal(state.tokenBalance).add(
          new Decimal(action.payload.tokenAmount)
        ),
        buyTokenError: undefined,
      };
    case buyTokensAct.FAILURE:
      return {
        ...state,
        buyTokenError: action.payload,
      };
    case buyTokensAct.FULFILL:
      return {
        ...state,
        isBuyingPending: false,
      };

    default:
      return state;
  }
}
