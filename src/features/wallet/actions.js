import { createRoutine } from 'redux-saga-routines';

export const getWalletAct = createRoutine('GET_WALLET');
export const generateWalletAct = createRoutine('GENERATE_WALLET');
export const buyTokensAct = createRoutine('BUY_TOKENS');

export const setWalletData = data => ({
  type: 'SET_WALLET_DATA',
  payload: data,
});
