import { request } from '../../services';

export const generateWalletRequest = (pin) => {
    return request.get(`/wallet/generate?pwd=${pin}`);
};

export const buyTokensRequest = (pin, amount) => {
    return request.get(`/buy/token?pwd=${pin}&amount=${amount}`);
};