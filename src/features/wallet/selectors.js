import { createSelector } from 'reselect';
import { Decimal } from 'decimal.js';

export const getWeiBalance = state => state.wallet.ethereumBalance;
export const getEthereumWallet = state => state.wallet.ethereumWallet;
export const getWeiTokenBalance = state =>
  new Decimal(state.wallet.tokenBalance).toString();

const convertWeiToEther = wei => {
  if (wei) {
    const dWei = new Decimal(wei);
    const divider = new Decimal('1000000000000000000');
    const dEth = dWei.dividedBy(divider);
    return dEth.toFixed(6).toString();
  }
  return '0.000000';
};

export const getEtherBalance = createSelector([getWeiBalance], weiBalance =>
  convertWeiToEther(weiBalance)
);

// export const getEtherTokenBalance = createSelector(
//   [getWeiTokenBalance],
//   weiTokenBalance => convertWeiToEther(weiTokenBalance)
// );
