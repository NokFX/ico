export const ActionTypes = {
    NAVIGATE: 'NAVIGATE',
}

export const navigate = (pathname) => ({
    type: ActionTypes.NAVIGATE,
    payload: {
        pathname,
    }
})