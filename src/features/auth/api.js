import { request } from '../../services';

export const authorize = (username, password) => {
  return request.urlencodedPost(
    '/oauth/token',
    {
      username,
      password,
      grant_type: 'password',
    },
    {
      Authorization: `Basic ${BASIC_TOKEN}`,
    }
  );
};

export const refreshToken = () => {
  const token = JSON.parse(localStorage.getItem('bc_ico'));
  if (token) {
    return request.urlencodedPost(
      '/oauth/token',
      {
        refresh_token: token.refresh_token,
        grant_type: 'refresh_token',
      },
      {
        Authorization: `Basic ${BASIC_TOKEN}`,
      }
    );
  }
};

export const currentUserRequest = () => {
  return request.get('/current/user');
};

export const storeToken = token =>
  localStorage.setItem('bc_ico', JSON.stringify(token));
export const clearToken = token => localStorage.removeItem('bc_ico');
export const getToken = () => JSON.parse(localStorage.getItem('bc_ico'));

export const verifyEmailRequest = email => {
  return request.jsonPost(
    '/verify/account',
    { email },
    {
      Authorization: `Basic ${BASIC_TOKEN}`,
    }
  );
};

export const registerRequest = (email, pwd, verifyCode) => {
  return request.jsonPost(
    '/create/account',
    { email, pwd, verifyCode },
    {
      Authorization: `Basic ${BASIC_TOKEN}`,
    }
  );
};

export const revokeTokenRequest = token => {
  return request.get('/oauth/revoke-token', {
    Authorization: `Bearer ${token}`,
  });
};
