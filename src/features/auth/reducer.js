import { login, loginByToken, logout, currentUserAct, verifyEmailAct, registerAct } from './actions';

 const initialState = {
    authenticated: false,
    isLoginPending: false,
    isCurrentUserPending: false,
    error: null,
    userData: {},

    registerData: {},
    isVerifyEmailPending: false,
    verifyEmailError: undefined,
    isEmailVerified: false,

    isRegisterPending: false,
    registerError: undefined,
 }

 export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case login.TRIGGER:
      return { 
          ...state,
          isLoginPending: true,
          error: undefined,
      };
    case login.SUCCESS:
      return { 
        ...state,
        authenticated: true,
        error: undefined,
      };
    case login.FAILURE:
      return { 
          ...state,
          error: action.payload,
      };
    case login.FULFILL:
      return {
        ...state,
        isLoginPending: false,
      };

    case loginByToken.TRIGGER:
      return { 
          ...state,
          isLoginPending: true,
          error: undefined,
      };
    case loginByToken.SUCCESS:
      return { 
        ...state,
        authenticated: true,
        error: undefined,
      };
    case loginByToken.FAILURE:
      return { 
          ...state,
          error: action.payload,
      };
    case loginByToken.FULFILL:
      return {
        ...state,
        isLoginPending: false,
      };  

    case currentUserAct.TRIGGER:
      return { 
          ...state,
          isCurrentUserPending: true,
          error: undefined,
      };
    case currentUserAct.REQUEST:
      return { 
          ...state,
          isCurrentUserPending: true,
          error: undefined,
      };
    case currentUserAct.SUCCESS: {
      return { 
        ...state,
        userData: action.payload,
      };
    }

    case currentUserAct.FAILURE:
      return { 
          ...state,
      };
    case currentUserAct.FULFILL:
      return {
        ...state,
        isCurrentUserPending: false,
      };

    case 'LOGOUT':
      return initialState;

    case verifyEmailAct.TRIGGER:
      return { 
        ...state,
        isVerifyEmailPending: true,
        verifyEmailError: undefined,
      };
    case verifyEmailAct.REQUEST:
      return { 
        ...state,
        registerData: action.payload,
      };
    case verifyEmailAct.SUCCESS:
      return { 
        ...state,
        isEmailVerified: true,
        verifyEmailError: undefined,
      };
    case verifyEmailAct.FAILURE:
      return { 
        ...state,
        verifyEmailError: action.payload,
      };
    case verifyEmailAct.FULFILL:
      return {
        ...state,
        isVerifyEmailPending: false,
      };  

    case registerAct.TRIGGER:
      return { 
        ...state,
        isRegisterPending: true,
        registerError: undefined,
      };
    case registerAct.SUCCESS:
      return { 
        ...state,
        registerData: {},
        isEmailVerified: false,
        registerError: undefined,
      };
    case registerAct.FAILURE:
      return { 
        ...state,
        registerError: action.payload,
      };
    case registerAct.FULFILL:
      return {
        ...state,
        isRegisterPending: false,
      };   
    default:
      return state;
  }
}

// import {
//   loginRequest,
//   loginSuccess,
//   loginError,
//   logout,
// } from './actions';

//  const defaultState = {
//     authenticated: false,
//     isLoginPending: false,
//     error: null,
//  }

//  const reducer = handleActions({
//     [loginRequest](state) {
//       return { 
//           ...state,
//           isLoginPending: true,
//           error: undefined,
//       };
//     },
//     [loginSuccess](state) {
//       return { 
//         ...state,
//         authenticated: true,
//         isLoginPending: false,
//       };
//     },
//     [loginError](state, payload) {
//       return { 
//           ...state,
//           isLoginPending: false,
//           error: payload,
//       };
//     },
//     [logout](state, payload) {
//       return defaultState;
//     },
//   }, defaultState);

//   export default reducer;