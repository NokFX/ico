import React, {Component} from 'react';
import {Link, Switch, Route, Redirect} from 'react-router-dom';
import {Container} from 'reactstrap';
import {ToastContainer, toast} from 'react-toastify';
import Header from '../../../../components/Header/';
import Sidebar from '../../../../components/Sidebar/';
import Breadcrumb from '../../../../components/Breadcrumb/';
import Aside from '../../../../components/Aside/';
import Footer from '../../../../components/Footer/';

import Dashboard from '../../../../views/Dashboard';
import Wallet from '../../../../views/Wallet';
import ICO from '../../../../views/ICO';

const containerStyle = {
  zIndex: 1999
};

class Full extends Component {
  render() {
    const { match } = this.props;

    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <Sidebar {...this.props}/>
          <main className="main mt-4">
            {/* <Breadcrumb /> */}
            <Container fluid>
              <Switch>
                <Route path={`${match.path}/dashboard`} name="Dashboard" component={Dashboard}/>
                <Route path={`${match.path}/wallet`} name="Wallet" component={Wallet}/>
                <Route path={`${match.path}/ico`} name="ICO" component={ICO}/>
              </Switch>
            </Container>
          </main>
          <Aside />
        </div>
        <Footer />
        <ToastContainer position="top-right" autoClose={5000} style={containerStyle}/>
      </div>
    );
  }
}

export default Full;
