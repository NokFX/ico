import React from 'react';
import { connect } from 'react-redux';
import {Route, Switch, Redirect} from 'react-router-dom';
import Login from '../../../../views/Login';
import Register from '../../../../views/Register';

const UnauthorizedLayout = ({
     location,
     match,
     authenticated,
 }) => {

    if (authenticated) {
        if (location.state && location.state.from ){
            return <Redirect to={location.state.from} />;
        }
        return <Redirect to="/app/dashboard" />;
    }

    return ( 
        <Switch>
            <Route path={`${match.path}/login`} name="Login" component={Login}/>
            <Route path={`${match.path}/register`} name="Login" component={Register}/>
            <Redirect to={`${match.path}/login`} />
        </Switch>
    )
};

const stateToProps = ({ auth }) => ({
    authenticated: auth.authenticated,
  })
  
export default connect(stateToProps)(UnauthorizedLayout)

