import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

class AuthorizedRoute extends Component {

    render() {
      const { component: Component, isLoginPending, authenticated, ...rest } = this.props;

      return (
        <Route {...rest} render={props => {
          if (isLoginPending) return <div />
          return authenticated
            ? <Component {...props} />
            : <Redirect to={{
              pathname: '/auth/login',
              state: { from: props.location }
            }} />
        }} />
      )
    }
  }
  
  const stateToProps = ({ auth }) => ({
    isLoginPending: auth.isLoginPending,
    authenticated: auth.authenticated,
  })
  
  export default connect(stateToProps)(AuthorizedRoute)