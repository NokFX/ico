import { createRoutine } from 'redux-saga-routines';

export const login = createRoutine('LOGIN');
export const loginByToken = createRoutine('LOGIN_BY_TOKEN');
export const currentUserAct = createRoutine('CURRENT_USER');
export const verifyEmailAct = createRoutine('VERIFY_EMAIL');
export const registerAct = createRoutine('REGISTER');
export const refreshTokenAct = createRoutine('REFRESH_TOKEN');
export const revokeTokenAct = createRoutine('REVOKE_TOKEN');

export const logout = () => ({
  type: 'LOGOUT',
});
