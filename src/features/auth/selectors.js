
export const getIsVerifyEmailPending = state => state.auth.isVerifyEmailPending;
export const getVerifyEmailError = state => state.auth.verifyEmailError;
export const getIsEmailVerified = state => state.auth.isEmailVerified;
export const getRegisterData = state => state.auth.registerData;
export const getIsRegisterPending = state => state.auth.isRegisterPending;
export const getRegisterError = state => state.auth.registerError;