import {
  take,
  all,
  call,
  put,
  cancelled,
  cancel,
  fork,
  takeEvery,
  select,
  race,
} from 'redux-saga/effects';
import { delay } from 'redux-saga';
import * as authApi from './api';
import {
  login,
  loginByToken,
  logout,
  currentUserAct,
  verifyEmailAct,
  registerAct,
  refreshTokenAct,
} from './actions';
import { setWalletData } from '../wallet/actions';
import { setTokenData } from '../coinData/actions';
import { getRegisterData } from './selectors';
import { actions as idleActions } from '../idle-monitor';
import { navigate } from '../navigation/actions';
import { getUpdateTokenPeriod } from '../../services/token';
import { getETHPriceOnce, getTokenRemainderOnce } from '../coinData/saga';

function* authorize(user, password) {
  try {
    yield put(login.request());
    const token = yield call(authApi.authorize, user, password);
    yield call(authApi.storeToken, token);
    yield* currentUser();
    //yield* getETHPriceOnce();
    yield* getTokenRemainderOnce();
    yield put(login.success());
    yield put(idleActions.start());
  } catch (error) {
    yield call(authApi.clearToken);
    yield put(login.failure(error));
  } finally {
    if (yield cancelled()) {
      // special cancellation handling code here
    }
    yield put(login.fulfill());
  }
}

function* authorizeByToken() {
  try {
    yield put(loginByToken.request());
    yield* currentUser();
    //yield* getETHPriceOnce();
    yield* getTokenRemainderOnce();
    yield put(loginByToken.success());
    yield put(idleActions.start());
  } catch (error) {
    yield call(authApi.clearToken);
    yield put(loginByToken.failure(error));
  } finally {
    if (yield cancelled()) {
      // special cancellation handling code here
    }
    yield put(loginByToken.fulfill());
  }
}

function* currentUser(isMainAuthProccess = false) {
  try {
    yield put(currentUserAct.request());
    const response = yield call(authApi.currentUserRequest);
    yield put(setWalletData(response));
    yield put(setTokenData(response));
    yield put(
      currentUserAct.success({
        id: response.id,
        email: response.email,
      })
    );
  } catch (error) {
    throw error;
  } finally {
    yield put(currentUserAct.fulfill());
  }
}

export function* watchlogin() {
  while (true) {
    const loginAction = yield take(login.trigger);
    const { login: user, password } = loginAction.payload;
    // fork return a Task object
    const task = yield fork(authorize, user, password);

    const action = yield take(['LOGOUT', login.FAILURE]);
    console.log('action', action);
    if (action.type === 'LOGOUT') {
      yield cancel(task);
      const token = yield call(authApi.getToken);
      yield fork(authApi.revokeTokenRequest, token.access_token);
    }
    yield call(authApi.clearToken);
    yield put(idleActions.stop());
  }
}

export function* watchLoginByToken() {
  while (true) {
    yield take(loginByToken.trigger);
    const task = yield fork(authorizeByToken);

    const action = yield take(['LOGOUT', loginByToken.FAILURE]);
    if (action.type === 'LOGOUT') {
      yield cancel(task);
      const token = yield call(authApi.getToken);
      yield fork(authApi.revokeTokenRequest, token.access_token);
    }
    yield call(authApi.clearToken);
    yield put(idleActions.stop());
  }
}

function* verifyEmail(actionBody) {
  try {
    yield put(verifyEmailAct.request(actionBody.payload));
    const response = yield call(
      authApi.verifyEmailRequest,
      actionBody.payload.email
    );
    yield put(verifyEmailAct.success(response));
  } catch (error) {
    yield put(verifyEmailAct.failure(error));
  } finally {
    yield put(verifyEmailAct.fulfill());
  }
}

function* register(actionBody) {
  try {
    yield put(registerAct.request());
    const regData = yield select(getRegisterData);
    const response = yield call(
      authApi.registerRequest,
      regData.email,
      regData.password,
      actionBody.payload
    );
    yield put(
      login.trigger({ login: regData.email, password: regData.password })
    );
    const action = yield take([login.SUCCESS, login.FAILURE]);
    if (action.type === login.FAILURE) {
      yield put(navigate, '/auth/login');
    } else {
      yield put(registerAct.success(response));
    }
  } catch (error) {
    yield put(registerAct.failure(error));
  } finally {
    yield put(registerAct.fulfill());
  }
}

function* refreshToken() {
  try {
    yield put(refreshTokenAct.request());
    localStorage.setItem('bc_ico_refreshing', true);
    const token = yield call(authApi.refreshToken);
    yield call(authApi.storeToken, token);
    yield put(refreshTokenAct.success());
  } catch (error) {
    yield put(refreshTokenAct.failure(error));
  } finally {
    localStorage.removeItem('bc_ico_refreshing');
    yield put(refreshTokenAct.fulfill());
  }
}

function* watchRefreshToken() {
  while (true) {
    const action = yield take([
      refreshTokenAct.SUCCESS,
      login.SUCCESS,
      loginByToken.SUCCESS,
    ]);

    if (action.type !== loginByToken.SUCCESS) {
      // в этом случае токен только что обновился, выставляем время следующего обновления чуть меньше чем token.expires_in
      // иначе, если loginByToken.SUCCESS, то когда токен обновился не знаем, обновим его сразу
      const { logoutEvent } = yield race({
        delayRefresh: delay(getUpdateTokenPeriod()),
        logoutEvent: take('LOGOUT'),
      });

      if (!logoutEvent) {
        yield race([call(refreshToken), take('LOGOUT')]);
      }
    } else {
      yield race([call(refreshToken), take('LOGOUT')]);
    }
  }
}

export function* watchAuth() {
  yield all([
    fork(watchlogin),
    fork(watchLoginByToken),
    fork(watchRefreshToken),
    takeEvery(currentUserAct.TRIGGER, currentUser),
    takeEvery(verifyEmailAct.TRIGGER, verifyEmail),
    takeEvery(registerAct.TRIGGER, register),
  ]);
}
