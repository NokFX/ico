import {
  IDLESTATUS_AWAY,
  IDLESTATUS_INACTIVE,
  IDLESTATUS_EXPIRED,
} from './constants';
import { logout } from '../auth/actions';
import { getTokenExpiresIn } from '../../services/token';

export const idleStatusDelay = idleStatus => (dispatch, getState) => {
  // время последующего таймера начинается после окончания предыдущего
  const expiresIn = getTokenExpiresIn();
  if (idleStatus === IDLESTATUS_AWAY) {
    return 10000;
  }
  if (idleStatus === IDLESTATUS_INACTIVE) {
    return 20000;
  }
  if (idleStatus === IDLESTATUS_EXPIRED) {
    return expiresIn > 30000 ? expiresIn - 20000 - 10000 : 1;
  }
};

export const activeStatusAction = (dispatch, getState) => {};

export const idleStatusAction = idleStatus => (dispatch, getState) => {
  if (idleStatus === IDLESTATUS_AWAY) {
  }
  if (idleStatus === IDLESTATUS_INACTIVE) {
  }
  if (idleStatus === IDLESTATUS_EXPIRED) {
    dispatch(logout());
  }
};
