import { getETHPriceAct, getTokenRemainderAct } from './actions';
import { Decimal } from 'decimal.js';

const initialState = {
  eth: {
    ethPrice: undefined,
  },
  token: {},
  error: undefined,
};

export default function transactionsReducer(state = initialState, action) {
  switch (action.type) {
    case getETHPriceAct.TRIGGER:
      return {
        ...state,
        error: undefined,
      };
    case getETHPriceAct.SUCCESS:
      return {
        ...state,
        eth: {
          ...state.eth,
          ethPrice: action.payload,
        },
        error: undefined,
      };
    case getETHPriceAct.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case getETHPriceAct.FULFILL:
      return {
        ...state,
      };

    case getTokenRemainderAct.TRIGGER:
      return {
        ...state,
        error: undefined,
      };
    case getTokenRemainderAct.SUCCESS:
      return {
        ...state,
        token: {
          ...state.token,
          tokenRemainder: action.payload,
        },
        error: undefined,
      };
    case getTokenRemainderAct.FAILURE:
      return {
        ...state,
        error: action.payload,
      };
    case getTokenRemainderAct.FULFILL:
      return {
        ...state,
      };

    case 'SET_TOKEN_DATA': {
      const {
        tokenBalance,
        tokenCode,
        tokenName,
        totalSupply,
        usdPrice,
      } = action.payload;

      return {
        ...state,
        token: {
          tokenBalance,
          tokenCode,
          tokenName,
          totalSupply,
          usdTokenPrice: usdPrice,
        },
      };
    }

    default:
      return state;
  }
}
