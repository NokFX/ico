import {
  all,
  fork,
  call,
  race,
  put,
  take,
  cancelled,
  cancel,
} from 'redux-saga/effects';
import { delay } from 'redux-saga';
import { Decimal } from 'decimal.js';
import * as api from './api';
import { getETHPriceAct, getTokenRemainderAct } from './actions';
import { login, loginByToken, logout } from '../auth/actions';

export function* getETHPriceOnce() {
  try {
    yield put(getETHPriceAct.request());
    const response = yield call(api.getETHPriceRequest);
    yield put(getETHPriceAct.success(response.price));
  } catch (error) {
    yield put(getETHPriceAct.failure(error));
  } finally {
    yield put(getETHPriceAct.fulfill());
  }
}

export function* getTokenRemainderOnce() {
  try {
    yield put(getTokenRemainderAct.request());
    const response = yield call(api.getTokenRemainderRequest);
    yield put(getTokenRemainderAct.success(response.currentSupply));
  } catch (error) {
    yield put(getTokenRemainderAct.failure(error));
  } finally {
    yield put(getTokenRemainderAct.fulfill());
  }
}

function* getETHPrice() {
  try {
    while (true) {
      try {
        yield put(getETHPriceAct.request());
        const response = yield call(api.getETHPriceRequest);
        yield put(getETHPriceAct.success(response.price));
      } catch (error) {
        yield put(getETHPriceAct.failure(error));
      } finally {
        yield put(getETHPriceAct.fulfill());
        if (!(yield cancelled())) {
          yield call(delay, 10000);
        }
      }
    }
  } finally {
    if (yield cancelled()) {
    }
  }
}

function* getTokenRemainder() {
  try {
    while (true) {
      try {
        yield put(getTokenRemainderAct.request());
        const response = yield call(api.getTokenRemainderRequest);
        yield put(getTokenRemainderAct.success(response.currentSupply));
      } catch (error) {
        yield put(getTokenRemainderAct.failure(error));
      } finally {
        yield put(getTokenRemainderAct.fulfill());
        if (!(yield cancelled())) {
          yield call(delay, 60000);
        }
      }
    }
  } finally {
    if (yield cancelled()) {
    }
  }
}

function* watchETHPrice() {
  while (yield take([login.SUCCESS, loginByToken.SUCCESS])) {
    const bgSyncTask = yield fork(getETHPrice);
    yield take('LOGOUT');
    yield cancel(bgSyncTask);
  }
}

function* watchTokenRemainder() {
  while (yield take([login.SUCCESS, loginByToken.SUCCESS])) {
    const bgSyncTask = yield fork(getTokenRemainder);
    yield take('LOGOUT');
    yield cancel(bgSyncTask);
  }
}

export function* watchCoinData() {
  yield all([fork(watchTokenRemainder)]);
}
