import { createSelector } from 'reselect';
import { Decimal } from 'decimal.js';

export const getTokenCode = state => state.wallet.tokenCode;
export const getUsdTokenPrice = state => state.coinData.token.usdTokenPrice;
export const getWeiTotalTokenSupply = state => state.coinData.token.totalSupply;
export const getTokenRemainder = state => state.coinData.token.tokenRemainder;

const convertWeiToEther = wei => {
  if (wei) {
    const dWei = new Decimal(wei);
    const divider = new Decimal('1000000000000000000');
    const dEth = dWei.dividedBy(divider);
    return dEth.toFixed(6).toString();
  }
  return '0.000000';
};

// export const getETHPrice = state =>
//   new Decimal(state.coinData.eth.ethPrice || 0).toString();

export const getETHPrice = state => new Decimal('1073').toString();

// export const getETHViewPrice = createSelector([getETHPrice], ethFullPrice =>
//   new Decimal(ethFullPrice).toFixed(2).toString()
// );

export const getETHViewPrice = state => new Decimal('1073').toString();

export const getEtherTokenPrice = createSelector(
  [getUsdTokenPrice, getETHPrice],
  (usdTokenPrice, usdEthPrice) => {
    return new Decimal(usdTokenPrice / usdEthPrice).toFixed(6).toString();
  }
);

export const getEtherTotalTokenSupply = createSelector(
  [getWeiTotalTokenSupply],
  weiTotalTokenSupply => {
    return weiTotalTokenSupply;
  }
);

// export const getEtherTokenViewPrice = createSelector(
//   [getEtherTokenPrice],
//   etherTokenPrice => {
//     return new Decimal(etherTokenPrice).toFixed(6).toString();
//   }
// );

//   export const getEtherTokenBalance = createSelector(
//     [getWeiTokenBalance],
//     weiTokenBalance => convertWeiToEther(weiTokenBalance)
//   );
