import { request } from '../../services';

export const getETHPriceRequest = () => {
  return request.get('/currency/price?code=ETH');
};

export const getTokenRemainderRequest = () => {
  return request.get('/ico/processing');
};
