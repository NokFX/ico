import { createRoutine } from 'redux-saga-routines';

export const getETHPriceAct = createRoutine('GET_ETH_PRICE');
export const getTokenRemainderAct = createRoutine('GET_TOKEN_REMAINDER');

export const setTokenData = data => ({
  type: 'SET_TOKEN_DATA',
  payload: data,
});
