import { call, put, takeEvery } from 'redux-saga/effects'
import { Decimal } from 'decimal.js';
import * as api from './api';
import { getTransactionsAct } from './actions';

function* getTransactions() {
    try {
        yield put(getTransactionsAct.request());
        const response = yield call(api.getTransactionsRequest);
        yield put(getTransactionsAct.success(response.list));
    } catch (error) {
        yield put(getTransactionsAct.failure(error));
    } finally {
        yield put(getTransactionsAct.fulfill());
    }
}

export function* watchTransactions() {
    yield takeEvery(getTransactionsAct.TRIGGER, getTransactions);
}