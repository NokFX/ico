import { createSelector } from 'reselect';
import { Decimal } from 'decimal.js';

export const getIsTransactionsPending = state =>
  state.transactions.isTransactionsPending;
export const getError = state => state.transactions.error;
export const getTransactionsData = state => state.transactions.data || [];

const compareTransactions = (t1, t2) => {
  return new Date(t2.date).getTime() - new Date(t1.date).getTime();
};
export const getTransactions = createSelector(
  [getTransactionsData],
  transactions => {
    const transacts = transactions.filter(t => !!t.amount).map(t => ({
      ...t,
      amount: new Decimal(t.amount)
        .dividedBy('1000000000000000000')
        .toFixed(6)
        .toString(),
    }));
    return transacts.sort(compareTransactions);
  }
);
