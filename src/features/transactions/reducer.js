import { getTransactionsAct } from './actions';
import { Decimal } from 'decimal.js';

 const initialState = {
     isTransactionsPending: false,
     error: undefined,
     data: [],
 }

 export default function transactionsReducer(state = initialState, action) {
  switch (action.type) {
    case getTransactionsAct.TRIGGER:
      return { 
        ...state,
        isTransactionsPending: true,
        error: undefined,
      };
    case getTransactionsAct.SUCCESS:
      return { 
        ...state,
        data: action.payload,
        error: undefined,
      };
    case getTransactionsAct.FAILURE:
      return { 
          ...state,
          error: action.payload,
      };
    case getTransactionsAct.FULFILL:
      return {
        ...state,
        isTransactionsPending: false,
     }; 

    default:
      return state;
  }
}