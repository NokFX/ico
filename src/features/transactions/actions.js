import { createRoutine } from 'redux-saga-routines';

export const getTransactionsAct = createRoutine('GET_TRANSACTIONS');