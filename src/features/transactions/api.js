import { request } from '../../services';

export const getTransactionsRequest = () => {
    return request.get(`/transactions/history`);
};