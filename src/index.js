import 'react-hot-loader/patch';
import 'babel-polyfill';
import { AppContainer } from 'react-hot-loader';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import App from './App';
import configureStore from './common/configureStore';
import { history } from './services';
import * as authActions from './features/auth/actions';
// Styles
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss';
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss';
import 'ladda/dist/ladda-themeless.min.css';
import 'spinkit/css/spinkit.css';

const store = configureStore();

// TODO подумать над синхронизацией между вкладками
// https://stackoverflow.com/questions/20325763/browser-sessionstorage-share-between-tabs
// пока просто удаляем признак обновления токена при загрузке страницы
localStorage.removeItem('bc_ico_refreshing');

const token = localStorage.getItem('bc_ico');
if (token) {
  // TODO возможно разделить экшен login на 2
  store.dispatch(authActions.loginByToken());
}

const render = Component =>
  ReactDOM.render(
    <AppContainer>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Component />
        </ConnectedRouter>
      </Provider>
    </AppContainer>,
    document.getElementById('root')
  );

render(App);
if (module.hot) {
  module.hot.accept('./App', () => render(App));
}
