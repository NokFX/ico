import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { responsiveStoreEnhancer } from 'redux-responsive';
import createSagaMiddleware, { END } from 'redux-saga';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';
import { history } from '../services';
import { middleware as idleMiddleware } from '../features/idle-monitor';

export default function configureStore(initialState) {
  let middleware;

  const logger = createLogger({
    collapsed: true,
    predicate: (getState, action) => action.type.indexOf('IDLE_') === -1,
  });
  const sagaMiddleware = createSagaMiddleware();

  // set middleware for development or production build
  if (process.env.NODE_ENV === 'development') {
    middleware = [
      thunk,
      idleMiddleware,
      sagaMiddleware,
      reduxImmutableStateInvariant(),
      logger,
    ];
  } else {
    middleware = [thunk, idleMiddleware, sagaMiddleware];
  }

  // create store
  const store = createStore(
    connectRouter(history)(rootReducer),
    initialState,
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(routerMiddleware(history), ...middleware),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );

  // if (module.hot) {
  //   // Enable Webpack hot module replacement for reducers
  //   module.hot.accept('./rootReducer', () => {
  //     const nextRootReducer = require('./rootReducer').default
  //     store.replaceReducer(nextRootReducer)
  //   })
  // }
  // store.runSaga = sagaMiddleware.run

  sagaMiddleware.run(rootSaga);
  store.close = () => store.dispatch(END);

  return store;
}
