import { combineReducers } from 'redux';
import { responsiveStateReducer } from 'redux-responsive';

import auth from '../features/auth/reducer';
import wallet from '../features/wallet/reducer';
import transactions from '../features/transactions/reducer';
import { reducer as idle } from '../features/idle-monitor';
import coinData from '../features/coinData/reducer';

const rootReducer = combineReducers({
  auth,
  wallet,
  browser: responsiveStateReducer,
  transactions,
  idle,
  coinData,
});

export default function(state, action) {
  if (action.type === 'LOGOUT') {
    // возвращаем все редьюсеры в состояние по-умолчанию
    return rootReducer(undefined, action);
  }
  return rootReducer(state, action);
}
