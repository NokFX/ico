import { take, put, call, fork, select, all } from 'redux-saga/effects';
import { api, history } from '../services';
import { ActionTypes } from '../features/navigation/actions';
import { watchAuth } from '../features/auth/saga';
import { watchWallet } from '../features/wallet/saga';
import { watchTransactions } from '../features/transactions/saga';
import { watchCoinData } from '../features/coinData/saga';

// trigger router navigation via history
function* watchNavigate() {
    while(true) {
      const { payload: { pathname } } = yield take(ActionTypes.NAVIGATE);
      yield history.push(pathname);
    }
  }
  
export default function* root() {
    yield all([
      fork(watchAuth),
      fork(watchNavigate),
      fork(watchWallet),
      fork(watchTransactions),
      fork(watchCoinData),
    ])
  }