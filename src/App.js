import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import AuthorizedRoute from './features/auth/containers/AuthorizedRoute';
import UnauthorizedLayout from './features/auth/containers/UnauthorizedLayout';
import PrimaryLayout from './features/auth/containers/PrimaryLayout';

const App = () => (
 <Switch>
    <Route path="/auth" component={UnauthorizedLayout} />
    <AuthorizedRoute path="/app" component={PrimaryLayout} />
    <Redirect to="/app/dashboard"/>
  </Switch>
);

export default App;